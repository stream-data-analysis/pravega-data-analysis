#!/usr/bin/env python3

import os, sys, argparse
import traceback, datetime

sys.path.append(os.pardir)
sys.path.append(".")

from utils.config_util import *
from utils.pravega_utils import *
from utils.k8s_utils import *
from utils.influx_utils import InfluxDBUtil


def set_and_get_options() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Automation test steps.')
    parser.add_argument('-i', '--id', action="store_true", help='generate new test identifier')
    parser.add_argument('-t', '--tiller', action='store_true', help="configure helm & tiller pod")
    group1 = parser.add_argument_group()
    group1.add_argument('--manifests', action="store_true", help='Prepare manifests for Pravega cluster components')
    group1.add_argument('-p', '--pravega', action="store_true", help='deploy Pravega cluster components')
    group1.add_argument('-b', '--benchmark', action="store_true", help='deploy benchmark (Mongoose)')
    group1.add_argument('-m', '--monitor', action="store", metavar=('ID', 'comment'), nargs='+',
                        help='deploy monitoring components')
    group2 = parser.add_argument_group()

    group2.add_argument('--start', action="store", metavar=('ID', 'DEFAULTS', 'SCENARIO'), nargs=3,
                        help='start benchmark with SCENARIO (path) and DEFAULTS (path)')
    group2.add_argument('--stop', action="store", metavar=('ID'),
                        help='stop benchmark (Mongoose scenario)')
    group2.add_argument('--time', action="store", metavar=('ID'),
                        help='get `start` & `end` time of test')
    group2.add_argument('--compare', action="store", metavar=('ID1', 'ID2'), nargs=2,
                        help='get `time shift` between 2 tests and generate link on comparision dashboard')
    group2.add_argument('-l', '--list', action="store_true",
                        help='list of all tests')
    group3 = parser.add_argument_group()
    group3.add_argument('-d', '--drop-series', action="store", metavar=('ID'),
                        help='drop series from influxDB with `test_id` = ID')
    group3.add_argument('-c', '--clear', action="store_true", help='clear cluster (delete all k8s resources)')
    group3.add_argument('-r', '--remove-pravega', action="store_true",
                        help='remove Pravega cluster (delete bk, controller, segmentstore). '
                             'NOTE: At this step, the Pravega cluster manifest (pravega.yml) is used, therefore the '
                             'cluster can only be removed if it was deployed with the `-p`'
                             'option and on the same working machine / container.')
    return parser.parse_args()


def set_comment_if_empty(comment):
    if comment.strip():
        return comment
    pretty_time = datetime.fromtimestamp(int(test_id) / 1000).strftime("%m/%d/%Y-%H:%M")
    return "-".join(["build", config.get_value("pravega.tag"), pretty_time])


if __name__ == '__main__':
    args = set_and_get_options()
    config = Config()
    db_host = config.get_value("db.host")
    db_port = config.get_value("db.port")
    db_db_data = config.get_value("db.db.data")
    db_db_meta = config.get_value("db.db.meta")
    db_url = f"http://{db_host}:{db_port}"
    influx = InfluxDBUtil(db_host, db_port, db_db_data, db_db_meta)
    try:
        if not check_helm():
            raise Exception("Helm isn't initialized.\nPlease, try to run `./test.py -t`")
        if args.id:
            print(generate_test_id())
        elif args.tiller:
            configure_helm_tiller()
        elif args.manifests:
            prepare_pravega_manifests(config)
        elif args.pravega:
            prepare_pravega_manifests(config)
            deploy_pravega(config)
            # to expose bookie ports
            deploy_pravega_bookie_svc()
        elif args.benchmark:
            image_tag = config.get_value("mongoose.tag")
            image_name = config.get_value("mongoose.image")
            deploy_mongoose_with_pravega_sd(image_name, image_tag)
        elif args.monitor:
            test_id = args.monitor[0]
            comment = ' '.join(args.monitor[1:])
            deploy_telegrafs(test_id, db_url, db_db_data)
            deploy_dashboard()
            comment = set_comment_if_empty(comment)
            influx.add_new_test_to_meta_db(test_id, comment)
        elif args.start:
            test_id = args.start[0]
            defaults = args.start[1]
            scenario = args.start[2]
            # Note: check that scenario and defaults exist
            if not (os.path.exists(defaults)):
                print(f"Path '{defaults}' dosn't exist. It will be used default path '{MONGOOSE_DEFAULTS_PATH}'")
                defaults = MONGOOSE_DEFAULTS_PATH
            if not (os.path.exists(scenario)):
                print(f"Path '{scenario}' dosn't exist. It will be used default path '{MONGOOSE_SCENARIO_PATH}'")
                scenario = MONGOOSE_SCENARIO_PATH
            configure_mongoose(defaults, scenario, test_id)
            start_mongoose(defaults, scenario, wait_until_finish=True)
        elif args.stop:
            stop_mongoose()
        elif args.drop_series:
            # Note: this option is useful to drop invalid test_id from InfluxDB
            test_id = args.drop_series
            influx.drop_series(test_id)
        elif args.time:
            # Note: this option is useful to get link on dashboard without Jenkins pipeline running
            id = args.time
            comment = influx.get_comment(id)
            end = influx.get_time(id)
            start = influx.get_time(id, last=False)
            print(start)
            print(end)
            vars = f"var-Test_alias={comment}&var-Test_id={id}" \
                   f"&from={start}&to={end}"
            print(f"""
            http://localhost:3000/d/zok43RcWkf/perfomance?orgId=1&{vars}
            """.strip())
        elif args.compare:
            ids = sorted(tuple(args.compare[0:2]))
            starts = list()
            ends = list()
            comments = list()
            for id in ids:
                starts.append(int(influx.get_time(test_id=id, last=False)))
                ends.append(int(influx.get_time(test_id=id)))
                comments.append(influx.get_comment(test_id=id))
            shift_millis = starts[1] - starts[0]
            shift = str(round(shift_millis / 1000)) + "s"
            start = starts[0] + shift_millis
            end = ends[0] + shift_millis
            vars = f"var-Test_alias={comments[0]}&var-Test_alias_2={comments[1]}" \
                   f"&var-Test_id={ids[0]}&var-Test_id_2={ids[1]}" \
                   f"&var-shift={shift}&var-shift_millis={shift_millis}" \
                   f"&from={start}&to={end}"
            print(f"""
            http://localhost:3000/d/zok43RcWkft/perfomance-over-the-tests?orgId=1&{vars}
            """.strip())
        elif args.list:
            print('%-35s %-25s %-35s' % ("TEST", "ID", "Test duration (sec)"))
            print("-------------------------------------------------------------------------------")
            durations = influx.get_test_durations()
            for l in influx.get_list_of_tests():
                id = l[1]
                color = COLORS.DEFAULT
                try:
                    d = durations[id]
                except:
                    d = "Undefined"
                    color = COLORS.RED
                print(color + '%-35s %-25s %-35s' % (l[0], id, d))
        elif args.clear:
            clear_cluster()
        elif args.remove_pravega:
            remove_pravega()
        sys.exit(0)
    except Exception as e:
        print(COLORS.RED + "Unexpected error:" + COLORS.DEFAULT)
        if not str(e):
            print("Error msg not found. \nTraceback:")
            traceback.print_exc()
        else:
            for msg in e.args:
                print(COLORS.RED + str(msg) + COLORS.DEFAULT)
        sys.exit(-1)
    finally:
        # NOTE: Trying to fix the empty line exceptions @ Jenkins
        sys.exit()
