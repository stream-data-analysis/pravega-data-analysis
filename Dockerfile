FROM telegraf:1.12.1-alpine

ADD config.yaml /config.yaml
ADD manifests /manifests
ADD utils /utils
ADD config.yaml /config.yaml
ADD requirements.txt /requirements.txt
ADD test.py /test.py
ADD config.yaml /config.yaml
ADD mongoose-defaults-example.yaml /mongoose-defaults-example.yaml
ENV KUBECONFIG ${HOME}/.kube/config
RUN chmod 755 ./utils/install_env.py
RUN apk add --no-cache --update \
        git \
        grep \
        wget \
        procps \
        python3 \
        py3-pip \
        sudo \
        tar \
        unzip \
        openssh-client \
        sshpass \
        ; \
    pip3 install --trusted-host pypi.org --trusted-host files.pythonhosted.org pip setuptools; \
    pip3 install --trusted-host pypi.org -r /requirements.txt; \
    python3 ./utils/install_env.py
ENTRYPOINT /bin/bash
