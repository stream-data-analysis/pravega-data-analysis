import groovy.transform.Field;

@Field final String testScriptFileName = "test.py";

final String generateTestId() {
    String testId = "";
    script {
        testId = sh(script: "python3 ${testScriptFileName} --id", returnStdout: true).trim();
    }
    return testId;
}

/**
* Deploys tiller of on Kubernetes cluster.
*/
void deployTillerPod(clusterManagerFileName, clusterName) {
    script {
        sh "python3 ${clusterManagerFileName} --use ${clusterName}"
        sh "python3 ${testScriptFileName} --tiller"
    }
}

void deployPravega(clusterManagerFileName, clusterName) {
    script {
        sh "python3 ${clusterManagerFileName} --use ${clusterName}"
        sh "python3 ${testScriptFileName} --pravega"
    }
}

void deployBenchmark(clusterManagerFileName, clusterName) {
    script {
        sh "python3 ${clusterManagerFileName} --use ${clusterName}"
        sh "python3 ${testScriptFileName} --benchmark"
    }
}

void deployMonitoring(testId, clusterManagerFileName, clusterName) {
    script {
        sh "python3 ${clusterManagerFileName} --use ${clusterName}"
        sh "python3 ${testScriptFileName} --monitor ${testId}"
    }
}

void startTest(clusterManagerFileName, testId, clusterName, scenarioPath = "", defaultsPath = "") {
    script {
        final String defaultEmptyPath = "-";
        if (scenarioPath == "") {
            scenarioPath = defaultEmptyPath;
        }

        if (defaultsPath == "") {
            defaultsPath = defaultEmptyPath;
        }
        sh "python3 ${clusterManagerFileName} --use ${clusterName}"
        sh "python3 ${testScriptFileName} --start ${testId} ${defaultsPath} ${scenarioPath}"
    }
}


void stopTest(testId, clusterName) {
    script {
        sh "python3 ${testScriptFileName} --stop ${testId}";
    }
}

return this;
