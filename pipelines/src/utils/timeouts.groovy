import groovy.transform.Field
/**
* In case @param body timed out, it continues the build with 'SUCCESS' status.
*/
final void executeWithSuccessOnTimeout(final int timeoutMinutes, Closure body) {
    try {
        catchError(buildResult: 'SUCCESS', stageResult: 'SUCCESS') {
            timeout(time: timeoutMinutes, unit: 'MINUTES') {
                body.call();
            }
        }
    } catch (Exception e) {
        print("Timeout has exceeded. Continuing pipeline execution with status 'success'.");
    }
}

return this;
