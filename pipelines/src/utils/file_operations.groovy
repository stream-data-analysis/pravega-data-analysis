import org.jenkinsci.plugins.pipeline.utility.steps.shaded.org.yaml.snakeyaml.scanner.ScannerException
import groovy.transform.Field


final String createFile(final String fileName, final String content) {
    // NOTE: Creating file with prefix in order to keep track of them.
    final String createdFilePrefix = "${env.BUILD_NUMBER}";
    final String creatingFileName = createdFilePrefix + fileName;
    writeFile(file: creatingFileName, text: content);
    return creatingFileName;
}

Object loadConfigurationFromYamlFile(final String filePath) {
    Object configuration = null;
    try {
        configuration = readYaml(file: "${filePath}");
    } catch (ScannerException invalidYamlException) {
        // NOTE: Terminating pipeline in case configuration hasn't been found on @filePath.
        final String invalidYamlErrorMsg = "YAML file at ${filePath} is invalid. Details: ${invalidYamlException}";
        error(invalidYamlErrorMsg);
    } catch (FileNotFoundException fileNotFoundException) {
        // NOTE: Handling situation when confuration file hasn't been found. Consider skip this ..
        // ... situation and create it manually?
        final String fileErrorMisleadingMsg = "Unable to find file on path ${filePath}. Details: ${fileNotFoundException}";
        error(fileErrorMisleadingMsg);
    } catch (Exception error) {
        final String configurationLoadErrorMsg = "Unable to load configuration from file ${filePath}. Details: ${error}";
        error(configurationLoadErrorMsg);
    }
    return configuration;
}

final Boolean saveYamlConfiguration(final String filePath, Object dataObject) {
    try {
        writeYaml(file: "${filePath}", data: dataObject);
    } catch (FileNotFoundException fileNotFoundException) {
        final String fileErrorMisleadingMsg = "Unable to find a file on path ${filePath}";
        echo "${fileErrorMisleadingMsg}"
        return false;
    } catch (Exception error) {
        final String errorMisleadingMsg = "Unable to save configuration into file ${filePath}, reason: ${error}. Configuration: ${dataObject}";
        echo "${errorMisleadingMsg}"
        return false;
    }
    return true;
}

final boolean doesDirectoryExist(final String path) {
    boolean doesExist = true;
    try {
        final String directoryContent = sh(script: "ls ${path}", returnStdout: true).trim();
        // NOTE: In case operation succeeded, directory exists.
        return doesExist;
    } catch (Exception error) {
        // NOTE: In case catch block has been reached, directory doesn't exist.
        doesExist = false;
    }
    return doesExist;
}

return this;
