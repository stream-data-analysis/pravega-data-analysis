import groovy.transform.Field
import groovy.json.JsonOutput

import java.text.SimpleDateFormat
import java.sql.Timestamp
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
import java.time.ZoneOffset


final String convertTimeToEpoch(final String time, final String timePattern) {
    SimpleDateFormat sdf = new SimpleDateFormat(timePattern);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern(timePattern);
    LocalDateTime dt = LocalDateTime.parse(time, dtf);
    Instant instant = dt.toInstant(ZoneOffset.UTC);
    final String epochTimestamp = instant.toEpochMilli();
    return epochTimestamp;
}

final String convertEpoch(final long epochTimestamp, final String pattern) {
    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    final String result = sdf.format(new Date(epochTimestamp)).toString();
    return result;
}

/** Displays red-colored error-tagged message. */
void displayErrorMsg(final String errorMsg) {
    final errorMsgTag = "Error";
    echo "\033[1;31m[$errorMsgTag]   \033[0m $errorMsg"
}

/** Displays yellow-colored info-tagged message. */
void displayInfoMsg(final String infoMsg) {
    final infoMsgTag = "Info";
    echo "\033[1;33m[$infoMsgTag]    \033[0m $infoMsg"
}

/** Displays green-colored success-tagged message. */
void displaySuccessMsg(final String successMsg) {
    final successMsgTag = "Success";
    echo "\033[1;32m[$successMsgTag] \033[0m $successMsg"
}

return this; 
