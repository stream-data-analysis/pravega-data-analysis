import java.text.NumberFormat

final Number getDifferenceInPercentage(Number rhs, Number lhs) {
    final Number diff = (rhs - lhs);
    final Number onePercent = rhs / 100;
    if (Math.abs(diff) < onePercent) {
        final boolean isNegative = (diff < 0);
        float onePercentValue = isNegative ? ((-1) * 0.01) : 0.01;
        return onePercentValue;
    }
    if (onePercent == 0.0) {
        // NOTE: In case previous value hasn't been found, return a zero value;
        return 0;
    }
    Number result = (diff / onePercent);

    final boolean isResultInfinite = Double.isInfinite(result);
    if (isResultInfinite) {
        print("The difference in percentage between ${rhs} and ${lhs} is infinite. Set it to zero.");
        // NOTE: If the different is infinite, assigning the zero number. In ...
        // ... that case, it won' be shown.
        result = 0.0;
    }

    return result;
}

final Number getStrValueAsNumber(final String value) {
    try {
        final Number num = NumberFormat.getInstance().parse(value);
        return num;
    } catch (NumberFormatException error) {
        echo "Unable to convert string value ${value} into a number."
    }
}

return this;
