Scripts on this repo are used to run deploy Pravega storage and run tests on k8s cluster.

# Contents.
1. [Metrics collection approach](#1-metrics-collection-approach)<br/>
2. [Environment](#2-environment)<br/>
&nbsp;&nbsp;2.1 [Docker](#31-docker)<br/>
3. [K8S manager](#4-k8s-manager-bare-metal)<br/>
&nbsp;&nbsp;3.1 [Usage](#41-usage)<br/>
&nbsp;&nbsp;3.2 [Available options](#42-available-options)<br/>
4. [Test case](#5-test-case)<br/>
&nbsp;&nbsp;4.1 [Available options](#51-available-options)<br/>
&nbsp;&nbsp;4.2 [Test case steps](#52-test-case-steps)<br/>
&nbsp;&nbsp;4.3 [Test case configuration](#53-test-case-configuration)<br/>
5. [Storage](#6-storage)<br/>
6. [Visualization](#7-visualization)<br/>
7. [Continuous delivery (Jenkins)](#8-continuous-delivery-jenkins)<br/>
&nbsp;&nbsp; 7.1 [Launch the test via Jenkins](#81-launch-the-test-via-jenkins)<br/>
8. [Troubleshooting](#9-troubleshooting)<br/>
&nbsp;&nbsp;8.1 [Unsupported docker v1 repository request](#91-unsupported-docker-v1-repository-request)<br/>
&nbsp;&nbsp;8.2 [Kubernetes CRDs not found for Pravega](#92-kubernetes-crds-not-found-for-pravega) <br/>
&nbsp;&nbsp;8.3[ SSL error on installing Python modules](#93-ssl-error-on-installing-python-modules)<br/>

# 1. Metrics collection approach

The SUT (System Under tests) principle is used for Pravega loading and collecting metrics.

All deployment steps and metrics collection are automated.

The components of the system, collected metrics and examples of steps for deployment are described below.

![](pravega-metrics.png)

List of collected metrics:

| Name | Kind | Prefix | Generator | Endpoint | Doc |
| ---  | ---  | ---    | ---       | ---      | --- |
| Benchmark metrics | External | `mongoose_` | Mongoose benchmark | `mongoose-entry-node-svc` | [link](https://github.com/emc-mongoose/mongoose-base/tree/master/doc/interfaces/api/remote#6-output) |
| System metrics | External | `cpu_`<br/> `mem_`<br/> `net_`<br/> `diskio_` | Telegraf DaemonSet | `tgnode-` pod | [cpu](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/cpu)<br/> [mem](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/mem)<br/> [net](https://github.com/influxdata/telegraf/blob/master/plugins/inputs/net/NET_README.md)<br/> [diskio](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/diskio) |
| System metrics | Internal | `jvm_` | Pravega | `pravega_controller-` pod | [link](https://github.com/pravega/pravega/blob/r0.5/documentation/src/docs/metrics.md#metrics-in-jvm) |
| Segmentstore metrics | Internal | `pravega_segmentstore_` | Pravega |  `pravega_segmentstore-` pod | [link](https://github.com/pravega/pravega/blob/r0.5/documentation/src/docs/metrics.md#metrics-in-segment-store-service) |
| Controller metrics | Internal | `pravega_controller_` | Pravega |  `pravega-controller-svc` | [link](https://github.com/pravega/pravega/blob/r0.5/documentation/src/docs/metrics.md#metrics-in-segment-store-service) |
| Bookkeeper metrics | Internal/External | `pravega_bookkeeper_` <br/> `bookie_` | Pravega/Bookie | `pravega-segmentstore-` pod<br/>`pravega-bookie-svc` | [pravega](https://github.com/pravega/pravega/blob/r0.5/documentation/src/docs/metrics.md#metrics-in-segment-store-service) <br/>[bk](https://bookkeeper.apache.org/docs/4.7.3/admin/metrics/) |


# 2. Environment
* Python 3
* Kubernetes 1.14.5 
* Jenkins 2.176.3 (optional)
* PKS 1.5.0 (optional)

> Note: to install test env `./utils/install_env.py`

# 2.1 Docker

It is **recomended to use** a prepared (or built) **docker image** in which all the necessary environment (DevKit) and scripts are installed.

To pull:
```
docker pull devops-repo.isus.emc.com:8116/perf-auto-env:latest
```

To build:
```
docker build -f Dockerfile -t perf-automation:<your_tag> .
```

To use:
```
docker run -it perf-automation:<your_tag> /bin/bash
```


### NOTE: *All next steps are performed inside the container.*
 
# 3. K8S manager (Bare metal)

The script `./utils/k8s_manager.py` may be used if test is run in Bare metal environment (Redhook cluster by default)

## 3.1 Usage

```
k8s_manager.py [-h] [-c] [-p] [-r] [-x]
```

## 3.2 Available options

To get actual list of all options run: `./utils/k8s_manager.py -h`

| Option | Short flag | Params | Description |
| --- | --- | --- | --- |
| --help | -h | - | help |
| --create | -c | - | create new k8s cluster with kubespray |
| --pv | -p | - | delete old and create new pv-s on cluster
| --reset | -r | - | reset k8s cluster with kubespray
| --context | -x | - | get context of existing baremetal cluster for kubectl
 
# 4. Run application

To run a single stage/step execute python script `app.py` with required option:

```
usage: app.py  [-h] [-i] [-t] [--manifests] [-p] [-b]
               [-m ID [comment ...]] [--start ID DEFAULTS SCENARIO]
               [--stop ID] [--time ID] [-d ID] [-c] [-r]
```

## 4.1 Available options

To get actual list of all options run: `./app.py -h`

| Option | Short flag | Params | Description |
| --- | --- | --- | --- |
| --help | -h | - | help |
| --id | -i | - | generate and return new test identifier |
| --tiller | -t | - | configure helm & tiller pod |
| --manifests| - | - | prepare manifests for Pravega cluster components (optional, as it is executed in `--pravega` step) |
| --pravega | -p | - | deploy Pravega cluster components |
| --benchmark | -b | - | deploy benchmark ([mongoose](https://github.com/emc-mongoose/mongoose))
| --monitor | -m |  [test-id] [comment] | deploy monitoring components; comment = briefly test description (by default <build>-<UTC time>) |
| --start | -| [test-id] [defaults-path] [scenario-path] | start benchmark with SCENARIO (path) and DEFAULTS (path) |
| --stop | - | [test-id] | stop benchmark (Mongoose scenario) |
| --time | - | [test-id] | get test time range (start & end timestamp in UTC milliseconds) |
| --compare | - | [test-id 1] [test-id 2] | generate link on Grafana comparision dashboard (compare 2 tests with specified test ids)
| --list | -l | - | list of all available tests |
| --drop-series | -d | [test-id] | drop metrics series from all DB with specified test ID |
| --remove-pravega | -r | - | remove Pravega cluster |
| --clear | -c | - | clear k8s cluster; remove all resources |

### 4.2 Application steps

> NOTE: Before starting the steps, be sure to [configure](#53-test-case-configuration) the scripts correctly.

#### 4.2.0 Test-ID generation. 

The identifier is used as a label for metrics and an `run-id` for a mongoose runs. It can be generated it using this command: 
```
ID=`python3 ./app.py -i`
```
and then it can be used as an argument in further steps.

The format is: `UNIX_EPOCH_TIME_UTC`

#### 4.2.1 Creation of new cluster with custom name and use them.

```
(Bare metal approach):
```
# ./utils/k8s_manager.py -r
./utils/k8s_manager.py -c
./utils/k8s_manager.py -p
./utils/k8s_manager.py -x
```

#### 4.2.2 Helm & tiller configuration. 

```
./app.py -t
```

#### 5.2.3 Deploy Pravega cluster and all necessary components on k8s cluster. 

These include:
* Storageclasses, pvc, rbac and other crd
* Zookeeper
* Bookeeper
* NFS
* Pravega operator + Pravega cluster

```
./app.py -p
```

#### 4.2.4 Deploy and configure monitoring components (k8s dashboard and telegraf with plugins).

Used plugins:

**Output**
* `prometheus_client` to export metrics to persistent DB (influx)

**Input**
* `cpu`
* `diskio`
* `net`
* `prometheus`
* `influxdb_listener` 

```
./app.py -m <ID> "my description"
```

#### 4.2.5 Deploy Benchmark (Load generator).

Mongoose + storage-driver-pravega is used as a benchmark. To configure Mongoose deployment (replicas count, image, tag) change `config.yaml`

```
./app.py -b
```

#### 4.2.6 Start Benchmark with some scenario.

Mongoose starts with scenario and defaults given as arguments. If paths don't exist then mongoose starts by default with `scenario.js` and `defaults.yaml` in `/tmp/pravega-performance/` directory.

```
./app.py --start <ID> /path/to/defautls.yaml /path/to/scenario.js
```

#### 4.2.7 Clear cluster. Deletion of all resources.
```
./app.py -c
```
#### 4.2.8 Deletion of k8s cluster.

(Bare metal approach):
```
./utils/k8s_manager.py --reset
```

## 4.3 Application configuration

To configure test case params is used `config.yaml` file.

To change Mongoose workload prepare `scenario.js` or/and `defaults.yaml` and use them as CLI arguments ([Start Benchmark with some scenario](#526-start-benchmark-with-some-scenario)).

# 5. Storage

All metrics are in InfluxDB http://TODO:8086.

> NOTE: The following metrics are not generated by the Pravega. To generate them, the CQs are used.
```
pravega_segmentstore_segment_read_bytes
pravega_segmentstore_segment_write_bytes
pravega_segmentstore_segment_write_events
pravega_segmentstore_segment_splits
pravega_segmentstore_segment_merge
pravega_segmentstore_storage_read_bytes
pravega_segmentstore_storage_write_bytes
pravega_segmentstore_bookkeeper_write_bytes
pravega_segmentstore_transactions_aborted
pravega_segmentstore_transactions_created
pravega_segmentstore_transactions_committed
pravega_segmentstore_stream_read_bytes
pravega_segmentstore_stream_write_bytes
pravega_segmentstore_stream_write_events
```

To generate additional pravega metrics, script `./utils/cq_util.py` can be used.
CQs are created once in the database and do not need to be recreated for each test (only if influxBD was redeployed or is created other DB).

## 5.1 Usage 
```
cq_utils.py [-h] [-l] [-c] [-d]
```
To get actual list of all options run: `./utils/cq_utils.py -h`

| Short flag | Option | Params | Description |
| --- | --- | --- | -- |
| -h, | --help   | [db name] | show this help message and exit |
| -l, | --list   | [db name] | list CQs |
| -c, | --create | [db name] | create all CQs |
| -d, | --delete | [db name] | delete all CQs |

> NOTE: database host and port can be changed in `config.yaml`

# 6. Visualization

Grafana is used to visualize metrics. Grafana is deployed on the same host http://localhost:3000 (admin/admin - if admin permissions are required).

| Dashboard | Link |
|---|---|
|Pravega Operation	| http://localhost:3000/d/qiRteDvZk/pravega-operation-dashboard
|Pravega Scope	| http://localhost:3000/d/KKZdJMxiz/pravega-scope-dashboard
|Pravega Stream |	http://localhost:3000/d/Kkgp6vvZz/pravega-stream-dashboard
|Pravega System	| http://localhost:3000/d/ViRtevvWz/pravega-system-dashboard
|Performance	| http://localhost:3000/d/zok43RcWkf/perfomance

A templates `grafana/[...].json` can be used for quick charting.
> [How import dashboard](https://grafana.com/docs/reference/export_import/#importing-a-dashboard)

All tests have `ID` ( == `$Test_id` variable) and `Test` ( == `$Test_alias` variable) and charts can be replotted using selection of `Test`.

![screenshot](doc/grafana_tetsts_list.PNG)

# 7. Continuous delivery (Jenkins)

## 7.1 Launch the test via Jenkins

1. <b>Navigate</b> to [performance test control host](). You could get credentials from [this page]().<br/>
2. <b>Create</b> a new project for your custom branch or select a [pre-defined one](). Note: it's preferrably to launch the test via Jenkins. <br/>
3. <b>Set</b> parameters and launch, following the instruction below: <br/>
![](pipelines/content/1-how-to-launch-with-parameters.PNG)

# 8. Troubleshooting 

## 8.1 Unsupported docker v1 repository request
In case you're using older versions of Docker, you'd probably receive a similar error: <br/>
```
Error: Status 400 trying to pull repository repo/image: "{\n  \"errors\" : [ {\n    \"status\" : 400,\n    \"message\" : \"Unsupported docker v1 repository request for 'repository-url'\"\n  } ]\n}"

``` 
In order to fix it, you should perform the following steps: <br/>
```
$ systemctl status docker
```
The output would look like this: <br/>
```
$ systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; disabled; vendor preset: disabled)
....
```
Copy the docker service description path. In this example, it's ```/usr/lib/systemd/system/docker.service```. The path may vary, depends on the system. In the scope of this troubleshooting, we'll put it into an environment variable in order to exclude possible misunderstandings regarding the path:<br/>
```
$ export DOCKER_SERVICE_FILE_PATH="/usr/lib/systemd/system/docker.service"
```
Open the docker service description with your favorite editor:
```
$ vim $DOCKER_SERVICE_FILE_PATH
```
Navigate to <b>ExecStart</b> option. For example: </br>
```
...
Environment=PATH=/usr/libexec/docker:/usr/bin:/usr/sbin
ExecStart=/usr/bin/dockerd-current \
          --add-runtime docker-runc=/usr/libexec/docker/docker-runc-current \
          --default-runtime=docker-runc \
          ....
```
Add the ```--disable-legacy-registry=true``` key into the <b>ExecStart</b>. It'll look like this:</br>
```
...
ExecStart=/usr/bin/dockerd-current \
          --add-runtime docker-runc=/usr/libexec/docker/docker-runc-current \
          --default-runtime=docker-runc \
          --disable-legacy-registry=true \
          ....
```
Save the file, exit it and apply the changes:</br>
```
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```

## 8.2 Kubernetes CRDs not found for Pravega
In case you've added your custom Pravega Operator, you could get an error: <br/>
```
 error: unable to recognize "~/pravega.yml": no matches for kind "PravegaCluster" in version "pravega.pravega.io/v              1alpha1"
```
Since Pravega requires Custom Resource Definitions (CRDs), it won't start without them. Pravega Operator, as an owner, should install those CRDs itself. But, it's possible that: <br/>
* Kubernetes will try to deploy Pravega Cluster before the CRDs are created; <br/>
* Your custom operator will be missing CRDs for deprecated API versions; <br/>
Either way, to fix this error, try the following steps: <br/>
1. Download Pravega Operator's CRDs (both [actual](https://github.com/pravega/pravega-operator/tree/master/deploy/crd.yml) and [deprecated](https://github.com/pravega/pravega-operator/tree/master/deloy/crds)); <br/>
2. Create those CRDs: <br/>
```
$ kubectl apply -f ~/deploy/crd.yml 
$ kubectl apply -f ~/deploy/crds
```

## 8.3 SSL error on installing Python modules
While installing Python modules, you may get the similar error: <br/>
```
$ pip3 install pyyaml
...
Retrying (Retry(total=1, connect=None, read=None, redirect=None, status=None)) after connection broken by 'SSLError(SSLError(1, '[SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:852)'),)': /simple/pyyaml/
```
You should consider adding ```pip3 install pyyaml --trusted-host pypi.org``` key to  it. In this xample "pypi.org" is the module provider. <b>WARNING: That may cause secutiry issues. Make sure to double-check whether you should trust the host or not.</b>
