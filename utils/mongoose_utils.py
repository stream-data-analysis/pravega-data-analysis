from utils.k8s_utils import *
from utils.constants import *

import urllib.request, requests, time, yaml, sys

MONGOOSE_DEFAULTS = """
run:
  id: {run_id}
storage:
  namespace: scope1

# NOTE: Setting Mongoose run to be executing for 15 minutes
load:
  step:
    limit:
      time: 15m
  
"""

MONGOOSE_SCENARIO = """
Load.run()

"""

MONGOOSE_DEFAULTS_PATH = PATHS.TMP_DIR + "/defaults.yaml"
MONGOOSE_SCENARIO_PATH = PATHS.TMP_DIR + "/scenario.js"


def deploy_mongoose(params=""):
    release_name = "mongoose"
    command = f"""
    helm install -n {release_name} emc-mongoose/mongoose-service {params}
    """
    execute(command)


def deploy_mongoose_with_pravega_sd(image_name, image_tag, replicas=1):
    benchmark_deploy_misleading_msg = "Deploying Benchmark {name}:{tag}".format(name=image_name, tag=image_tag)
    info_msg(benchmark_deploy_misleading_msg)
    try:
        controller_ip = get_pod_ip("pravega-pravega-controller").rstrip('\r\n').strip()
        # controller_ip = "pravega-pravega-controller"
    except:
        controller_ip = "pravega-controller"
    params = """ \
    --set image.name={image_name} \
    --set image.tag={image_tag} \
    --set "args=\"--storage-net-node-addrs={ip}\"" \
    --set replicas={replicas} \
    --set resources.requests.memory="2Gi"  \
    --set resources.limits.memory="4Gi" \
    --set resources.requests.cpu="1500m"
    """.format(ip=controller_ip,
               image_tag=image_tag,
               image_name=image_name,
               replicas=replicas)
    deploy_mongoose(params)


def get_mongoose_url(minikube_used=False) -> str:
    ip = get_resource_ip(MONGOOSE_K8S_SERVICE_NAME, minikube=minikube_used)
    port = get_resource_port(MONGOOSE_K8S_SERVICE_NAME, minikube=minikube_used)
    return "http://{ip}:{port}".format(ip=ip, port=port)


def start_mongoose(defaults=None, scenario=None, minikube_used=False, wait_until_finish=False):
    url = get_mongoose_url(minikube_used) + "/run"
    params = None
    if (defaults != None) and (scenario != None):
        params = {'defaults': open(defaults, 'rb'), 'scenario': open(scenario, 'rb')}
    time.sleep(5)
    requests.post(url=url, files=params)
    info_msg("Mongoose scenario has started")

    if (not wait_until_finish):
        return
    # NOTE: Wait some time until Mongoose server starts
    time.sleep(50)
    # NOTE: Mongoose status is being checked based on ID
    print(COLORS.GREEN + "\nWait for the mongoose scenario to finish" + COLORS.DEFAULT)
    try:
        mongoose_run_id = get_mongoose_run_id()
    except KeyError as run_id_error:
        print(run_id_error)
        sys.exit(-1)
    is_mongoose_running = True
    while (is_mongoose_running):
        try:
            # NOTE: Not terminating the script until Mongoose run finishes.
            is_mongoose_running = is_mongoose_run_in_progress(url=url, run_id=mongoose_run_id)
        except KeyboardInterrupt:
            # NOTE: Allows user to terminate benchmark state monitoring on sig kill.
            info_msg("Mongoose scenario will continue to run in the background")
            sys.exit()
    info_msg("Mongoose scenario has finished.")



def is_mongoose_run_in_progress(url, run_id, minikube_used=False) -> bool:
    # NOTE: Mongoose checks status for run within the custom 'If-Match' header.
    run_status_headers = {'If-Match': run_id}
    run_status_request = urllib.request.Request(url=url, headers=run_status_headers)
    mongooose_run_status = urllib.request.urlopen(run_status_request).status
    # NOTE: Mongoose returns status 200 if run is still in progress.
    found_mongoose_run_status = 200
    is_mongoose_benhcmark_active = bool(mongooose_run_status == found_mongoose_run_status)
    return is_mongoose_benhcmark_active


def get_mongoose_run_id(minikube_used=False) -> str:
    '''Returns Mongoose run ID gathered via REST API, empty value ...
    ... in case of failure.'''
    url = get_mongoose_url(minikube_used) + "/run"
    response = requests.head(url=url)
    # NOTE: Mongoose Run ID is mapped to 'ETag' response header.
    mongoose_run_id_matching_header = 'ETag'
    result = ""
    try:
        result = response.headers[mongoose_run_id_matching_header]
    except KeyError:
        raise KeyError("{} hasn't been found within Mongoose response.".format(mongoose_run_id_matching_header))
    except:
        raise Exception("Unable to get Mongoose run ID from HTTP response.")
    return result


def stop_mongoose(minikube_used=False):
    url = get_mongoose_url(minikube_used) + "/run"
    run_id = get_mongoose_run_id()
    headers = {"If-Match": run_id}
    requests.delete(url=url, headers=headers)


def wait_for_mongoose():
    info_msg("Wait for mongoose-node(s)")
    mongoose_url = get_mongoose_url() + "/config"
    print(COLORS.GREEN + "Mongoose URL: " + mongoose_url + COLORS.DEFAULT)
    while True:
        try:
            conn = urllib.request.urlopen(mongoose_url)
            if None != conn:
                code = conn.getcode()
                print(COLORS.GREEN + "Mongoose service response code " + str(code) + COLORS.DEFAULT)
                if 200 == code:
                    break
            else:
                time.sleep(10)
        except Exception as e:
            print(e)
            time.sleep(10)
    info_msg("Mongoose service is up and running")


def configure_mongoose(defaults, scenario, test_id):
    info_msg("Configure mongoose")
    # NOTE: add run-id to defaults.yaml
    if os.path.exists(defaults):
        with open(defaults, 'r') as stream:
            content = yaml.load(stream.read(), Loader=yaml.FullLoader)
        if 'run' not in content.keys():
            content['run'] = {'id': test_id}
        else:
            content['run']['id'] = test_id
        with open(defaults, 'w') as yaml_file:
            yaml_file.write(yaml.dump(content, default_flow_style=False))
    else:
        create_file(defaults, MONGOOSE_DEFAULTS.format(run_id=test_id))
    # NOTE: check scenario
    if not os.path.exists(scenario):
        create_file(scenario, MONGOOSE_SCENARIO)
    # NOTE: print content of files to stdout
    with open(defaults, 'r') as fin:
        print("Mongoose defaults:\n\t" + fin.read().replace("\n", "\n\t"))
    with open(scenario, 'r') as fin:
        print("Mongoose scenario:\n\t" + fin.read().replace("\n", "\n\t"))
    wait_for_mongoose()
