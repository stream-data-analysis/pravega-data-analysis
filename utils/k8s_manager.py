#!/usr/bin/env python3

import os, sys, argparse

sys.path.append(os.pardir)
sys.path.append(".")

from utils.config_util import *
from utils.k8s_utils import *


def get_ssh_line(config, scp=False) -> str:
    login = config.get_value("baremetal.login")
    password = config.get_value("baremetal.password")
    ip = config.get_value("baremetal.master_ip")
    if scp:
        return """sshpass -p {p} scp -o stricthostkeychecking=no -r {l}@{i}""".format(l=login,
                                                                                      p=password,
                                                                                      i=ip)
    return """sshpass -p {p} ssh -o stricthostkeychecking=no {l}@{i}""".format(l=login,
                                                                               p=password,
                                                                               i=ip)


def get_kube_context(config):
    ssh_line = get_ssh_line(config, scp=True)
    kube_path = config.get_value("baremetal.kube_path")
    cmd = ssh_line + ":{path} $HOME".format(path=kube_path)
    execute(cmd)


def install_metallb():
    metallb("apply")


def delete_metallb():
    metallb("delete")


def metallb(command):
    execute("kubectl " + command + " -f " + ARTIFACTORY.METAL_LB)
    execute("kubectl " + command + " -f " + MANIFESTS.METAL_LB_CM)


def reset(config, retry=False):
    flag = ""
    if retry:
        flag = """--limit @/opt/kubespray/reset.retry"""
    ssh_line = get_ssh_line(config)
    cmd = ssh_line + """ 'cd /opt/kubespray/; ansible-playbook -i inventory/redhook/inventory.ini \
                                                               --become --become-user=root \
                                                               %s \
                                                               reset.yml \
                                                               -e "confirmation=yes"'""" % flag
    execute(cmd)
    print(COLORS.BLUE +
          "If there is a problem with the reset on some nodes, then try to do it on problem node:\nrm -rf /var/lib/etcd/"
          + COLORS.DEFAULT)


def is_reset_succ():
    ssh_line = get_ssh_line(config)
    cmd = ssh_line + """ 'cat /opt/kubespray/reset.retry'"""
    limit = execute(cmd, return_stdout=True).strip()
    if limit is None or "":
        return True
    return False


def create_pv(config):
    ssh_line = get_ssh_line(config)
    cmd = ssh_line + """ 'cd /opt/local-pv && ./create-pvs.sh'"""
    execute(cmd)


def patch_pv():
    pvs = execute("""kubectl get pv --no-headers --template '{{range .items}}{{.metadata.name}}{{"\\n"}}{{end}}'""",
                  return_stdout=True)
    for pv in pvs.split():
        cmd = """kubectl patch pv %s -p '{"metadata":{"finalizers":null}}'""" % pv
        execute(cmd)


def create_cluster(config):
    ssh_line = get_ssh_line(config)
    cmd = ssh_line + """ 'cd /opt/kubespray/; ansible-playbook -i inventory/redhook/inventory.ini \
                                                    --become --become-user=root \
                                                    cluster.yml;'"""
    execute(cmd)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Manage baremetal cluster.')
    parser.add_argument('-c', '--create', action="store_true", help='create new k8s cluster with kubespray')
    parser.add_argument('-p', '--pv', action="store_true", help='delete old and create new pv-s on cluster')
    parser.add_argument('-r', '--reset', action="store_true", help='reset k8s cluster with kubespray')
    parser.add_argument('-m', '--metallb', action="store_true", help='delete metalLB from cluster')
    parser.add_argument('-x', '--context', action="store_true",
                        help='get context of existing baremetal cluster for kubectl')

    args = parser.parse_args()
    config = Config()
    if args.create:
        create_cluster(config)
    elif args.pv:
        patch_pv()
        create_pv(config)
    elif args.reset:
        reset(config)
        if not is_reset_succ():
            reset(config, retry=True)
    elif args.metallb:
        delete_metallb()
    elif args.context:
        get_kube_context(config)
        install_metallb()
        configure_helm_tiller()
