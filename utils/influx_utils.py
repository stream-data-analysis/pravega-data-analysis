from utils.common_utils import *

import requests, sys

sys.path.append(os.pardir)
sys.path.append(".")

from dateutil import tz
from influxdb import InfluxDBClient


class InfluxDBUtil:

    def __init__(self, host, port, data_db, meta_db):
        self.host = host
        self.port = port
        self.data_db = data_db
        self.meta_db = meta_db

    def __get_client__(self, db) -> InfluxDBClient:
        """
        Return InfluxDBClient instance
        :param db: DB (bucket) name
        :return: InfluxDBClient instance
        """
        try:
            return InfluxDBClient(host=self.host, port=self.port, database=db)
        except requests.ConnectionError as e:
            raise Exception(f'Can\'t connect to PerfDB: {self.host}:{self.port}\n', e.args)

    def get_time(self, test_id, last=True) -> str:
        """
        Return last/first timestamp of measurement
        :param test_id: unique test identifier
        :return: timestamp in milliseconds of last metric point in DB
        """
        c = self.__get_client__(self.data_db)
        try:
            measurement = "mongoose_success_op_rate_last"
            if last:
                r = c.query(f'select value from {measurement} where "test_id" = \'{test_id}\' order by desc limit 1;')
            else:
                r = c.query(f'select value from {measurement} where "test_id" = \'{test_id}\' order by asc limit 1;')
            raw = r.raw['series'][0]['values'][0][0]
        except Exception as e:
            raise Exception("Series with test ID `%s` not found" % test_id)

        influx_data_pattern = "%Y-%m-%dT%H:%M:%SZ"
        # detect zones:
        from_zone = tz.tzutc()
        to_zone = tz.tzlocal()
        utc = datetime.strptime(raw, influx_data_pattern)
        utc = utc.replace(tzinfo=from_zone)
        # Convert time zone
        local_time = utc.astimezone(to_zone)
        return str(int(local_time.timestamp() * 1000))

    def get_comment(self, test_id) -> str:
        """
        Return test comment (used as alias)
        :param test_id: unique test identifier
        :return: comment field for test from meta DB
        """
        db = self.meta_db
        c = self.__get_client__(db)
        try:
            query = f"""SHOW SERIES ON "{db}" WHERE "test_id" = '{test_id}'"""
            results = c.query(query).raw['series'][0]['values']
            measurement = results[0][0].split(',')[0]
        except Exception as e:
            raise Exception("Series with test ID `%s` not found" % test_id)
        return measurement

    def add_new_test_to_meta_db(self, test_id, comment) -> None:
        """
        Add info about new test run to meta bucket in InfluxDB
        :param test_id: unique test identifier
        :param comment: user metadata for briefly test description (used as test alias)
        :return: None
        """
        db = self.meta_db
        c = self.__get_client__(db)
        try:
            data = f"""{comment},test_id={test_id},comment='{comment}' value={test_id},comment="{comment}" """
            params = {"db": db}
            c.write(data, params=params, protocol="line")
        except Exception as e:
            raise Exception("Can't write to PerfDB", e.args)

    def drop_series(self, test_id) -> None:
        """
        Drop all series tagged with `test_id` from meta- and data- db
        :param test_id: unique test identifier
        :return:
        """
        try:
            self.__drop_series__(self.meta_db, test_id)
            self.__drop_series__(self.data_db, test_id)
        except:
            self.__drop_series__(self.data_db, test_id)
            raise

    def __drop_series__(self, db, test_id) -> None:
        """
        Drop all series tagged with `test_id` from specified DB (bucket)
        :param db: name of database
        :param test_id: unique test identifier
        :return:

        NOTE: refactoring is needed, but there are blocker issue: https://github.com/influxdata/influxdb-python/issues/767
        """
        info_msg("Drop series from " + db)
        c = self.__get_client__(db)
        try:
            query = f"""SHOW measurements ON "{db}" WHERE "test_id" = '{test_id}'"""
            results = c.query(query).raw['series'][0]['values']
            measurements = set()
            for r in results:
                measurements.add(r[0].split(',')[0])
            for m in measurements:
                print(m + " was deleted")
                c.delete_series(db, m, {"test_id": test_id})
        except Exception as e:
            raise Exception("Can't delete series from PerfDB with test_id=" + test_id, e.args)

    def get_test_durations(self):
        """
        Return dict of mongoose_elapsed_time (in seconds) {"test_id" : "elapsed_time"}
        :return: dict of test duration in sec
        """
        db = self.data_db
        q = """
        SELECT max("value") FROM "mongoose_elapsed_time_value" GROUP BY "test_id"
        """
        c = self.__get_client__(db)
        results = c.query(q).raw['series']
        d = dict()
        for r in results:
            k = r['tags']["test_id"]
            v = r['values'][0][1]
            d[k] = v
        return d

    def get_list_of_tests(self) -> list:
        """
        Retern list of [<comment>, <test id>]
        :return: list of lists
        """
        db = self.meta_db
        c = self.__get_client__(db)
        query = f"""SHOW series ON "{db}" """
        results = c.query(query).raw['series'][0]['values']
        return [[r[0].split(',')[0], r[0].split(',')[2].replace("test_id=", "")] for r in results]
