import os
import re
import subprocess
import urllib.request

from datetime import *
from utils.constants import *


def generate_test_id() -> str:
    """
    :return: UTC in milliseconds
    """
    now = datetime.utcnow().timestamp() * 1000
    test_id = str(int(now))
    return test_id


def execute(command, return_stdout=False, as_root=False) -> any:
    """
    Execute shell command
    :param command: command content
    :param return_stdout: if True, return stdoutput of command, else return nothing
    :param as_root: if True, execute command with sudo permissions
    :return: stdoutput or None
    """
    cmd_msg(command)

    if as_root:
        command = 'sudo sh -c \"%s\"' % (command)
    if return_stdout:
        result = subprocess.run([command], shell=True, stdout=subprocess.PIPE)
        return result.stdout.decode('utf-8')
    subprocess.run([command], shell=True)


def unzip(s_path, d_path=".") -> None:
    """
    Extract .zip archives
    :param s_path: source path
    :param d_path: destination path
    :return None
    """
    execute("unzip -d {d} {s}".format(s=s_path, d=d_path))


def untargz(s_path, d_path=".") -> None:
    """
    Extract .tar.gz archives
    :param s_path: source path
    :param d_path: destination path
    :return None
    """
    execute("tar -xvzf {s} -C {d}".format(s=s_path, d=d_path))


def download(url, path, file_name) -> None:
    """
    Download a file
    :param url: URL
    :param path: destination path
    :param file_name: destination file name
    :return: None
    """
    os.makedirs(path, exist_ok=True)
    if path[-1] != "/":
        path = path + "/"
    urllib.request.urlretrieve(url, path + file_name)


def add_to_hosts(ip, name) -> None:
    """
    Add hostname and IP address to /etc/hosts
    :param ip: IP address
    :param name: Hostname
    :return: None
    """
    p = re.compile(ip + "\s*" + name)
    for line in open("/etc/hosts", "r").readlines():
        if p.match(line):
            return  # do not add if there is already such a line
    command = "echo '{ip}     {name}' >> /etc/hosts".format(ip=ip, name=name)
    execute(command, as_root=True)


def create_file(full_name, content, print_content=False) -> None:
    """
    Create or replace file
    :param full_name: path + file name of final file
    :param content: file content
    :param print_content: if True, file content is printed to stdout
    :return: None
    """
    print(COLORS.GREEN + "Prepare the " + full_name + " file" + COLORS.DEFAULT)
    if print_content:
        print(COLORS.BLUE + f"Content of {full_name}:\n" + COLORS.DEFAULT + content)
    with open(full_name, "w+") as file:
        file.write(content)


def get_resource_ip(name, kind="service", namespace="default", minikube=False) -> str:
    """
    Get and return k8s resource IP address
    :param name: resource name
    :param kind: kind of resource (pod, svc ...)
    :param namespace: resource namespace
    :param minikube: set True, if minikube cluster is used
    :return: IP address string
    """
    if minikube:
        command = "minikube ip"
    else:
        command = """
        kubectl describe {k} {n} -n {ns} """.format(k=kind, n=name, ns=namespace)
        command += ' | grep "LoadBalancer Ingress:" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n1'
    ip = execute(command, return_stdout=True).strip()
    if not ip:
        raise Exception(f"It's impossible to get ip address of `{name}`")
    return ip


def get_resource_port(name, kind="service", namespace="default", port_name="http", minikube=False) -> str:
    """
    Get and return exposed port of k8s resource
    :param name: resource name
    :param kind: kind of resource (pod, svc ...)
    :param namespace: resource namespace
    :param minikube: set True, if minikube cluster is used
    :return: Port string
    """
    if minikube:
        command = """
            kubectl describe {k} {n} -n {ns} | grep NodePort:[[:blank:]]*{p} | sed 's/[^0-9]*//g'
            """.format(k=kind, n=name, ns=namespace, p=port_name)
    else:
        command = """
        kubectl describe {k} {n} -n {ns} | grep ^Port:[[:blank:]]*{p} | sed 's/[^0-9]*//g'
        """.format(k=kind, n=name, ns=namespace, p=port_name)
    return execute(command, return_stdout=True).strip()


def get_id_token() -> str:
    """
    Get and return kubernetes id-token of current context
    :return: id-token string
    """
    out = execute("kubectl config view | grep id-token", return_stdout=True)
    return out.split(":")[1]


def kill_process(name) -> None:
    """
    Kill named unix process
    :param name: name of process
    :return: None
    """
    out = execute('ps aux | grep "{}"'.format(name), return_stdout=True).strip()
    print(out)
    pid = out.split()[1]
    print("PID: " + pid)
    execute("kill -9 " + pid)


def info_msg(txt) -> None:
    """
    Print pretty info msg to stdout
    :param txt: Printed text msg
    :return: None
    """
    row = len(txt) + 2
    h = ''.join(['+'] + ['-' * row] + ['+'])
    result = h + '\n'"| " + txt + " |"'\n' + h
    print(COLORS.GREEN + result + COLORS.DEFAULT)


def cmd_msg(cmd) -> None:
    """
    Print pretty msg about executed shell commands
    :param cmd: Executed cmds
    :return: None
    """
    print(COLORS.MAGENTA + "Executed commands:" + COLORS.DEFAULT)
    cmds = cmd.splitlines()
    for c in cmds:
        c = c.strip()
        if not c: continue
        print(COLORS.MAGENTA + "  $ " + c + COLORS.DEFAULT)
