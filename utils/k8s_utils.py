from utils.common_utils import *
from utils.influx_utils import *

import time
import requests, time
import kubernetes as k8s

SLEEP = 20


def get_k8s_api() -> k8s.client.CoreV1Api:
    """
    Get anf return k8s client api.
    :return: k8s client
    """
    # Configs can be set in Configuration class directly or using helper utility
    k8s.config.load_kube_config()
    return k8s.client.CoreV1Api()


def get_k8s_extended_api() -> k8s.client.ApiextensionsV1beta1Api:
    """
    Get anf return extended k8s client api.
    :return: extended k8s client
    """
    k8s.config.load_kube_config()
    return k8s.client.ApiextensionsV1beta1Api(k8s.client.ApiClient())


def get_pod(name, ns=None) -> k8s.client.V1Pod:
    """
    Get and return pod instance
    :param name: name of k8s pod
    :param ns: namespace where pod is deployed
    :return: k8s pod instance
    """
    v1 = get_k8s_api()
    if ns is None:
        ret = v1.list_pod_for_all_namespaces(watch=False)
    else:
        ret = v1.list_namespaced_pod(namespace=ns, watch=False)
    for pod in ret.items:
        if name in pod.metadata.name:
            return pod
    raise Exception("Pod doesn't deployed: " + name)


def get_pod_ip(name, ns=None) -> str:
    """
    Get and return Pod IP address
    :param name: name of k8s pod
    :param ns: namespace where pod is deployed
    :return: IP address of pod
    """
    pod = get_pod(name, ns)
    return str(pod.status.pod_ip)


def get_pod_status(name, ns=None) -> str:
    """
    Get and return Pod status
    :param name: name of k8s pod
    :param ns: namespace where pod is deployed
    :return: status of pod
    """
    try:
        pod = get_pod(name, ns)
    except:
        time.sleep(20)
        pod = get_pod(name, ns)
    return str(pod.status.phase)


def print_statuses(resources, statuses) -> None:
    """
    Pretty print statuses of resources in stdout
    :param resources: list of resource names
    :param statuses: list of resource statuses
    :return: None
    """
    for r, s in zip(resources, statuses):
        print(COLORS.GREEN + f"\t{r}r : {s}" + COLORS.DEFAULT)


def get_pods_statuses(names, ns=None) -> list:
    """
    Get and return list of resources statuses
    :param names: list of resource names
    :param ns: namespace where all resources are deployed
    :return: list of statuses
    """
    statuses = list()
    for n in names:
        try:
            statuses.append(get_pod_status(n, ns))
        except:
            statuses.append("Doesn't deployed yet")
    return statuses


def check_crd(name) -> bool:
    """
    Check that named crd exists
    :param name: name of crd
    :return: True, if crd exists, else False
    """
    v1 = get_k8s_extended_api()
    ret = v1.list_custom_resource_definition(watch=False)
    for crd in ret.items:
        if name in crd.metadata.name:
            return True
    return False


def wait_for_pod(name, ns="default"):
    """
    Blocker function, that wait for pod will running
    :return: None
    """
    pod_status = get_pod_status(name, ns)
    while (pod_status != "Running"):
        print(COLORS.GREEN + f"Wait for {name} pod... Status: " + pod_status + COLORS.DEFAULT)
        time.sleep(SLEEP)
        pod_status = get_pod_status(name, ns)


def wait_for_crd(name):
    """
    Blocker function, that wait for crd will be created
    :return: None
    """
    while not check_crd(name):
        print(COLORS.GREEN + f"Wait for {name} CRD..." + COLORS.DEFAULT)
        time.sleep(SLEEP)
    info_msg(f"{name} CRD has been created!")


def add_repos_to_helm() -> None:
    """
    Add required repositories to local helm
    :return: None
    """
    info_msg("Add repos to helm")
    cmd = """
    helm repo add influx http://influx-charts.storage.googleapis.com
    helm repo add emc-mongoose https://emc-mongoose.github.io/mongoose-helm-charts/
    helm repo update
    """
    execute(cmd)


def init_tiller() -> None:
    """
    Simple tiller initialisation
    :return: None
    """
    info_msg("Init tiller & helm")
    cmd = """
    kubectl create serviceaccount --namespace kube-system tiller
    kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
    helm init --service-account tiller
    """
    execute(cmd)


def check_helm() -> bool:
    """
    Check that helm is initialized and repos were added
    :return: Boolean
    """
    info_msg("Check helm")
    cmd = """
    helm search mongoose
    """
    out = execute(cmd, return_stdout=True)
    if "Error" in out:
        return False
    return True


def configure_helm_tiller(init_function=init_tiller):
    """
    Configure local helm and tiller on current k8s cluster
    :param init_function: function refs to initialize tiller
    :return: None
    """
    info_msg("Configuring of tiller & helm")
    init_function()
    add_repos_to_helm()
    wait_for_pod("tiller", ns="kube-system")
    info_msg("Tiller configuration has finished.")


def dashboard() -> None:
    """
    Deploy proxy and print link + token
    :return:
    """
    kill_process("kubectl proxy")
    execute("kubectl proxy &")
    print(COLORS.GREEN + """
    Dashboard: http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy
    Token: """ + get_id_token() + COLORS.DEFAULT)


def deploy_dashboard() -> None:
    """
    Deploy k8s dashboard on current cluster
    :return:
    """
    command = """cd ../
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
    """
    execute(command)
    dashboard()


def deploy_gateway_telegraf() -> None:
    """
    Run the cluster internal "gateway" telegraf
    """
    execute("kubectl apply -f " + MANIFESTS.TELEGRAF_SVC)
    command = f"""
        helm install --name telegraf stable/telegraf \
            --set config.outputs[0].prometheus_client.listen=:9273 \
            --set config.inputs[0].prometheus.urls[0]=http://{MONGOOSE_K8S_SERVICE_NAME}:9999/metrics \
            --set config.inputs[0].prometheus.urls[1]=http://pravega-bookie-svc:8000/metrics \
            --set config.inputs[0].influxdb_listener.service_address=:8186 \
            --set config.outputs[0].file.files[0]="stdout" \
            --set service.enabled=false
        """
    execute(command)


def deploy_tgnodes(output) -> None:
    """
    Run the telegraf nodes to collect the cluster nodes CPU/mem/disk/net metrics
    :param output: output URL for influxdb plugin
    """
    command = f"""
        helm install --name tgnodes influx/telegraf-ds \
            --set config.outputs[0].influxdb.urls[0]=http://{output}:8186 \
            --set config.inputs[0].cpu.percpu=false \
            --set config.inputs[0].cpu.totalcpu=true \
            --set config.inputs[0].diskio.devices[0]=sda \
            --set config.inputs[0].diskio.devices[1]=sdb \
            --set config.inputs[0].net.interfaces[0]=eth* \
            --set config.inputs[0].mem=true
        """
    execute(command)


def deploy_local_telegraf(label, db_url, db, input) -> None:
    """
    Run "proxy" telegraf instance as unix process on local machine
    :param label: global label for tagging all measurements
    :param db_url: URL of destination DB
    :param db: Name of DB (bucket)
    :param input: input URL for metrics from cluster
    :return: None
    """
    tg_config = f"""
        [global_tags]
            test_id = "{label}"
        [[outputs.influxdb]]
            urls = [ "{db_url}" ]
            database = "{db}"
        [[inputs.prometheus]]
            urls = [ "http://{input}:9273/metrics" ]

        """
    create_file("/etc/telegraf/telegraf.conf", tg_config, print_content=True)
    kill_process("telegraf")
    # Run local telegraf
    execute("nohup telegraf &")
    print(execute("ps alx | grep telegraf"))


def deploy_telegrafs(label, db_url, db) -> None:
    """
    Deploy all stack of telegrafs (Proxy, Gateway and DaemonSet)
    :param label: global label for tagging all measurements
    :param db_url: URL of destination DB
    :param db: Name of DB (bucket)
    :return: None
    """
    # Run the cluster internal "gateway" telegraf
    deploy_gateway_telegraf()
    # Determine the "gateway" telegraf external IP address
    out = execute("kubectl get services | grep telegraf", True)
    tg_info = out.split()
    tg_internal_addr = tg_info[2]
    tg_external_addr = tg_info[3]
    print(f"Internal cluster telegraf internal addres: {tg_internal_addr}, external: {tg_external_addr}")
    # Run the telegraf nodes to collect the cluster nodes CPU/mem/disk/net metrics
    deploy_tgnodes(output=tg_internal_addr)
    # Run local telegraf witch scrapes metrics from `input` and push them to perfDB
    deploy_local_telegraf(label, db_url, db, input=tg_external_addr)


def deploy_sandbox() -> None:
    f"""
    Deploy sandbox with exposed ports (from {PATHS.RESOURCE_DIR + "/sandbox.yaml"})
    :return: None
    """
    execute("kubectl apply -f " + PATHS.RESOURCE_DIR + "/sandbox.yaml")


def remove_pravega() -> None:
    f"""
    Remove PravegaCluster crd from {PATHS.TMP_DIR + "/pravega.yml"} manifest
    :return: None
    """
    pravega_manifest_path = PATHS.TMP_DIR + "/pravega.yml"
    if os.path.exists(pravega_manifest_path):
        cmd = "kubectl delete -f " + pravega_manifest_path
        execute(cmd)
    else:
        print(pravega_manifest_path + " doesn't exist. Try to clear whole cluster.")





def delete_resource(resource, namespace="default") -> None:
    """
    Delete k8s resource
    :param resource: name of resource
    :param namespace: namespace where resource is deployed
    :return: None
    """
    execute(f"kubectl delete --all --force --grace-period=0 --wait=false -n {namespace} {resource}")


def clear_helm() -> None:
    """
    Delete all helm releases
    :return: None
    """
    execute("helm del --purge $(helm ls --all --short)")


def clear_pv_pvc(namespace) -> None:
    """
    Delete all pv and pvc from specified namespace
    :param namespace: namespace where pv and pvc are deployed
    :return: None
    """
    delete_resource("pvc", namespace)
    delete_resource("pv", namespace)


def clear_unremoved(namespace) -> None:
    """
    Delete all remaining resources from specified namespace
    :param namespace: namespace where resources are deployed
    :return: None
    """
    delete_resource("pods", namespace)
    delete_resource("svc", namespace)
    delete_resource("deployments", namespace)
    delete_resource("all", namespace)


def clear_crds(namespace) -> None:
    """
    Delete zookeepercluster and pravegacluster crd from specified namespace
    :param namespace: namespace where crds are deployed
    :return: None
    """
    delete_resource("zookeeperclusters", namespace)
    delete_resource("pravegaclusters", namespace)


def clear_from_manifests() -> None:
    """
    Delete resources which was deployed with manifests
    :return: None
    """
    dirs = [PATHS.RESOURCE_DIR, PATHS.TMP_DIR, PATHS.ZK_DIR, PATHS.PRAVEGA_OPERATOR_DEPLOY_DIR]
    for d in dirs:
        if os.path.exists(d):
            execute("kubectl delete -f " + d + " --wait=false --force --grace-period=0")


def force_clear_resources():
    f"""
       Remove k8s resources from current cluster
       :return: None
       """
    ns = ["default"]
    for n in ns:
        clear_pv_pvc(namespace=n)
        clear_unremoved(namespace=n)
        clear_crds(namespace=n)


def clear_cluster() -> None:
    """
    Clear all namespaces of current k8s cluster
    :return: None
    """
    info_msg("Clearing cluster ...")
    remove_pravega()
    clear_helm()
    clear_from_manifests()
    force_clear_resources()
