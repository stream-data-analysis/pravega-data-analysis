import glob
import shutil

from utils.mongoose_utils import *
from utils.constants import *


def deploy_pravega_bookie_svc(ns="default"):
    info_msg("Deploy bookie service to expose bookie metrics")
    cmd = f"kubectl apply -n {ns} -f {MANIFESTS.PRAVEGA_BOOKIE_SVC}"
    execute(cmd)


def prepare_zk_manifests(config):
    download(ARTIFACTORY.ZK_RBAC, PATHS.ZK_DIR, ARTIFACTORY.ZK_RBAC.split("/")[-1])
    download(ARTIFACTORY.ZK_OPERATOR, PATHS.ZK_DIR, ARTIFACTORY.ZK_OPERATOR.split("/")[-1])
    download(ARTIFACTORY.ZK_CLUSTER_CRD, PATHS.ZK_DIR, ARTIFACTORY.ZK_CLUSTER_CRD.split("/")[-1])
    replicas = config.get_value("zookeeper.replicas")
    create_file(PATHS.TMP_DIR + "/zk.yml", ZK_YAML.format(replicas))


def clone_pravega_operator(url):
    """
    Clones Pravega operator if it doesn't exist, deploys it afterwards.
    """
    pravega_operator_path = PATHS.PRAVEGA_OPERATOR_DIR
    if os.path.exists(pravega_operator_path):
        command = """
        cd {dir}
        git pull
        cd -
        """.format(dir=pravega_operator_path)
        execute(command)
    else:
        execute("git clone " + url + " " + pravega_operator_path)


def prepare_pravega_manifests(config):
    info_msg("Prepare manifests for Pravega Cluster deployment")
    # Prepare zk
    prepare_zk_manifests(config)
    # Prepare pravega operator
    clone_pravega_operator(url=config.get_value("pravega.operator.url"))
    # Prepare Tier2
    create_file(PATHS.TMP_DIR + "/pravega-tier2.yml", TIER_2_YAML, print_content=True)
    # Prepare Pravega Cluster
    create_file(PATHS.TMP_DIR + "/pravega.yml",
                PRAVEGA_YAML.format(PRAVEGA_REPO=config.get_value("pravega.repo"),
                                    PRAVEGA_SC_NAME=config.get_value("pravega.storageClass.name"),
                                    PRAVEGA_TAG=config.get_value("pravega.tag"),
                                    PRAVEGA_CTRL_REPLICAS=config.get_value("pravega.controller.replicas"),
                                    PRAVEGA_SS_REPLICAS=config.get_value("pravega.segmentstore.replicas"),
                                    BK_REPO=config.get_value("bookkeeper.repo"),
                                    BK_TAG=config.get_value("bookkeeper.tag"),
                                    BK_LEDGER_STORAGE=config.get_value("bookkeeper.resources.storage.ledger"),
                                    BK_JOURNAL_STORAGE=config.get_value("bookkeeper.resources.storage.journal"),
                                    BK_REQ_MEM=config.get_value("bookkeeper.resources.requests.memory"),
                                    BK_REQ_CPU=config.get_value("bookkeeper.resources.requests.cpu"),
                                    BK_LIM_MEM=config.get_value("bookkeeper.resources.limits.memory"),
                                    BK_LIM_CPU=config.get_value("bookkeeper.resources.limits.cpu"),
                                    BK_REPLICAS=config.get_value("bookkeeper.replicas"),
                                    BK_SC_NAME=config.get_value("bookkeeper.storageClass.name"),
                                    INFLUX_DB_NAME=config.get_value("db.db.data")),
                print_content=True)
    info_msg("Manifests preparation is finished")


def get_pravega_resource_list(config) -> list:
    bk_replicas = config.get_value("bookkeeper.replicas")
    zk_replicas = config.get_value("zookeeper.replicas")
    ss_replicas = config.get_value("pravega.segmentstore.replicas")

    resources = [f"pravega-bookie-{n}" for n in range(bk_replicas)]
    resources = resources + [f"pravega-pravega-segmentstore-{n}" for n in range(ss_replicas)]
    resources = resources + [f"zookeeper-{n}" for n in range(zk_replicas)]
    return resources


def check_resources_and_wait(resources, bad_stats) -> bool:
    pod_stats = get_pods_statuses(resources)
    while any(s != "Running" for s in pod_stats):
        print_statuses(resources, pod_stats)
        if set(pod_stats).intersection(bad_stats) != set():
            return False
        time.sleep(SLEEP)
        pod_stats = get_pods_statuses(resources)
    print("\n")
    print_statuses(resources, pod_stats)
    return True


def check_pravega_cluster(config) -> bool:
    bad_stats = ["ErrImagePull", "ImagePullBackOff", "CrashLoopBackOff"]
    resources = get_pravega_resource_list(config)
    info_msg("\nWait for Pravega cluster components\n")
    return check_resources_and_wait(resources, bad_stats)


def check_nfs() -> bool:
    out = execute("helm ls --all --short", return_stdout=True)
    if "nfs" in out:
        return True
    return False


def deploy_storage_class():
    info_msg("Storage class deployment")
    execute("kubectl create -f " + MANIFESTS.STORAGE_CLASS)


def deploy_zk():
    info_msg("ZK operator deployment")
    command = f"kubectl create -f {PATHS.ZK_DIR}"
    execute(command)
    wait_for_crd("zookeeperclusters")
    execute("kubectl create -f " + PATHS.TMP_DIR + "/zk.yml")


def deploy_pravega_operator():
    info_msg("Pravega operator deployment")
    execute("kubectl create -f " + PATHS.PRAVEGA_OPERATOR_DEPLOY_DIR)


def deploy_tier_2(config):
    info_msg("Tier 2 storage deployment")
    nfs_server = config.get_value("nfs.server")
    nfs_path = config.get_value("nfs.path")
    command = """
        helm install --name nfs stable/nfs-client-provisioner \
                     --set nfs.server=%s \
                     --set nfs.path=%s \
                     --set storageClass.name=nfs \
                     --set nfs.mountOptions="{nolock,sec=sys,vers=4.0}"
        """ % (nfs_server, nfs_path)
    execute(command)
    if not check_nfs():
        raise Exception("NFS server doesn't deployed. Try to check helm & tiller.")
    info_msg("NFS server deployed as Tier 2")


def deploy_tier_2_volume():
    info_msg("Tier 2 volume deployment")
    execute("kubectl create -f " + PATHS.TMP_DIR + "/pravega-tier2.yml")


def redeploy_pravega_cluster() -> None:
    """
    Delete and redeploy only custom resource pravega.pravega.io/v1alpha1/PravegaCluster
    :return: None
    """
    execute("kubectl delete -f " + PATHS.TMP_DIR + "/pravega.yml")
    time.sleep(60)
    execute("kubectl create -f " + PATHS.TMP_DIR + "/pravega.yml")


def deploy_pravega_cluster(config) -> None:
    """
    Deploy only custom resource pravega.pravega.io/v1alpha1/PravegaCluster
    :return: None
    """
    info_msg("Pravega cluster deployment")
    wait_for_crd("pravegaclusters")
    execute("kubectl create -f " + PATHS.TMP_DIR + "/pravega.yml")


def deploy_pravega(config) -> None:
    """
    Deploy Pravega Cluster and all required components (ZK, StorageClass, Operators, NFS + Volume, BK...)
    :param config: test configuarion
    :return: None
    """
    deploy_storage_class()
    deploy_zk()
    deploy_pravega_operator()
    deploy_tier_2(config)
    deploy_tier_2_volume()
    deploy_pravega_cluster(config)
    # Check Pravega components
    if not check_pravega_cluster(config):
        redeploy_pravega_cluster()
    info_msg("Pravega is deployed!")



