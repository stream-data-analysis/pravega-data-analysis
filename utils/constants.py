# Variables

class COLORS:
    RED = '\033[31m'
    GREEN = '\033[32m'
    BLUE = '\033[94m'
    DEFAULT = '\033[0m'
    MAGENTA = '\033[95m'


class PATHS:
    RESOURCE_DIR = "./manifests"
    TMP_DIR = "/tmp/pravega"
    CA_DIR = TMP_DIR + "/ca"
    ZK_DIR = TMP_DIR + "/zk"
    PRAVEGA_OPERATOR_DIR = TMP_DIR + "/pravega-operator"
    PRAVEGA_OPERATOR_DEPLOY_DIR = PRAVEGA_OPERATOR_DIR + "/deploy"


class ARTIFACTORY:
    KUBECTL = "https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/linux/amd64/kubectl"
    HELM = "https://storage.googleapis.com/kubernetes-helm/helm-v2.13.1-linux-amd64.tar.gz"
    ZK_RBAC = "https://raw.githubusercontent.com/pravega/zookeeper-operator/master/deploy/default_ns/rbac.yaml"
    ZK_OPERATOR = "https://raw.githubusercontent.com/pravega/zookeeper-operator/master/deploy/default_ns/operator.yaml"
    ZK_CLUSTER_CRD = "https://raw.githubusercontent.com/pravega/zookeeper-operator/master/deploy/crds/zookeeper_v1beta1_zookeepercluster_crd.yaml"
    METAL_LB = "https://raw.githubusercontent.com/google/metallb/v0.8.1/manifests/metallb.yaml"


class MANIFESTS:
    SANDBOX = PATHS.RESOURCE_DIR + "/sandbox.yaml"
    TELEGRAF_SVC = PATHS.RESOURCE_DIR + "/telegraf-svc.yaml"
    NODE_EXPORTER = PATHS.RESOURCE_DIR + "/node-exporter.yaml"
    PROMETHEUS_SVC = PATHS.RESOURCE_DIR + "/prometheus-service.yaml"
    STORAGE_CLASS = PATHS.RESOURCE_DIR + "/storageclass.yaml"
    PRAVEGA_BOOKIE_SVC = PATHS.RESOURCE_DIR + "/pravega-bookie-svc.yaml"
    METAL_LB_CM = PATHS.RESOURCE_DIR + "/metallb-config.yaml"


MONGOOSE_K8S_SERVICE_NAME = "mongoose-entry-node-svc"

PRAVEGA_IMAGE = "pravega/pravega"
BOOKEEPER_IMAGE = "pravega/bookkeeper"

ZK_YAML = """
apiVersion: "zookeeper.pravega.io/v1beta1"
kind: "ZookeeperCluster"
metadata:
  name: "zookeeper"
spec:
  size: {}
  resources:
    requests:
      storage: 1Gi
      cpu: "1000m"
"""

TIER_2_YAML = """
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pravega-tier2
spec:
  storageClassName: "nfs"
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
"""

PRAVEGA_YAML = """
apiVersion: "pravega.pravega.io/v1alpha1"
kind: "PravegaCluster"
metadata:
  name: "pravega"
spec:
  zookeeperUri: zookeeper-0.zookeeper-headless.default.svc.cluster.local:2181

  bookkeeper:
    image:
      repository: "{BK_REPO}"
      tag: "{BK_TAG}"
      pullPolicy: Always

    replicas: {BK_REPLICAS}
    resources:
      requests:
        memory: {BK_REQ_MEM}
        cpu: {BK_REQ_CPU}
      limits:
        memory: {BK_LIM_MEM}
        cpu: {BK_LIM_CPU}
    storage:
      ledgerVolumeClaimTemplate:
        accessModes: [ "ReadWriteOnce" ]
        storageClassName: {BK_SC_NAME}
        resources:
          requests:
            storage: {BK_LEDGER_STORAGE}

      journalVolumeClaimTemplate:
        accessModes: [ "ReadWriteOnce" ]
        storageClassName: {BK_SC_NAME}
        resources:
          requests:
            storage: {BK_JOURNAL_STORAGE}

    autoRecovery: true

    options:
      journalSyncData: "false"
      useHostNameAsBookieID: "true"
      enableStatistics: "true"
      statsProviderClass: "org.apache.bookkeeper.stats.prometheus.PrometheusMetricsProvider"
      
      # - MARK: Experiments with Kubernetes Resource Management
      isForceGCAllowWhenNoSpace: "true"

      minorCompactionThreshold: "0.4"
      minorCompactionInterval: "1800"
      majorCompactionThreshold: "0.8"
      majorCompactionInterval: "43200"

  pravega:
    controllerReplicas: {PRAVEGA_CTRL_REPLICAS}
    # - MARK: Experiments with Kubernetes Resource Management
    controllerResources:
      requests:
        memory: "3Gi"
        cpu: "500m"
      limits:
        memory: "4Gi"
        cpu: "2000m"

    segmentStoreReplicas: {PRAVEGA_SS_REPLICAS}
    segmentStoreResources:
      requests:
        memory: "4Gi"
        cpu: "1000m"
      limits:
        memory: "8Gi"
        cpu: "4000m"

    cacheVolumeClaimTemplate:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: {PRAVEGA_SC_NAME}
      resources:
        requests:
          storage: 1Gi

    image:
      repository: "{PRAVEGA_REPO}"
      tag: "{PRAVEGA_TAG}"
      pullPolicy: Always

    tier2:
      filesystem:
        persistentVolumeClaim:
          claimName: pravega-tier2
          
    options:
      metrics.enableStatistics: "true"
      metrics.outputFrequencySeconds: "10"
      metrics.enableInfluxDBReporter: "true"
      metrics.influxDBURI: "http://telegraf.default:8186"
      metrics.influxDBName: "{INFLUX_DB_NAME}"
      metrics.influxDBRetention: "two_hour"

      controller.metrics.enableStatistics: "true"
      controller.metrics.outputFrequencySeconds: "10"
      controller.metrics.enableInfluxDBReporter: "true"
      controller.metrics.influxDBURI: "http://telegraf.default:8186"
      controller.metrics.influxDBName: "{INFLUX_DB_NAME}"
      controller.metrics.influxDBRetention: "two_hour"
    
      # - MARK: Experiments with Kubernetes Resource Management
      controller.containerCount: "32"
      controller.retention.bucketCount: "10"
      controller.service.asyncTaskPoolSize: "20"
      controller.retention.threadCount: "4"
      writer.flushThresholdBytes: "1048576"
      writer.maxFlushSizeBytes: "1048576"
      pravegaservice.containerCount: "32"
        
"""
