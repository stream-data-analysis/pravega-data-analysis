#!/usr/bin/env python3

import os
import sys
import subprocess

subprocess.call([sys.executable, "-m", "pip", "install", "-r", "/requirements.txt"])
sys.path.append(os.pardir)
sys.path.append(".")

from utils.common_utils import *


def install_env():
    print(COLORS.GREEN + "Update certificates" + COLORS.DEFAULT)
    command = """
    unzip -d /etc/ssl/certs {dir}/EMCCerts.zip
    unzip -d /usr/local/share/ca-certificates {dir}/EMCCerts.zip
    unzip -d /etc/pki/ca-trust/source/anchors/ {dir}/EMCCerts.zip
    unzip -d /etc/kubernetes/pki/ {dir}/EMCCerts.zip
    update-ca-certificates
    """.format(dir=PATHS.CA_DIR)
    execute(command, as_root=True)
    #
    print(COLORS.GREEN + "Install tools" + COLORS.DEFAULT)
    command = """
    cp {dir}/pks/* /usr/bin/pks && chmod +x /usr/bin/pks
    cp {dir}/kubectl/* /usr/bin/kubectl && chmod +x /usr/bin/kubectl
    cd {dir}/helm && tar -xzf helm* && cd -
    cp {dir}/helm/linux-amd64/helm /usr/local/bin/helm
    """.format(dir=PATHS.TMP_DIR)
    execute(command, as_root=True)
    #
    print(COLORS.GREEN + "\n\nREADY\n\n" + COLORS.DEFAULT)


def download_env():
    print(COLORS.GREEN + "Download certificates" + COLORS.DEFAULT)
    dirs = [PATHS.TMP_DIR, PATHS.CA_DIR,
            "/etc/pki/ca-trust/source/anchors/",
            "/usr/local/share/ca-certificates",
            "/etc/ssl/certs ",
            "/etc/kubernetes/pki/"]
    for dir in dirs:
        os.makedirs(dir, exist_ok=True)
    print(COLORS.GREEN + "Download tools" + COLORS.DEFAULT)
    download(ARTIFACTORY.KUBECTL, PATHS.TMP_DIR + "/kubectl", "kubectl")
    download(ARTIFACTORY.HELM, PATHS.TMP_DIR + "/helm", "helm")


if __name__ == '__main__':
    download_env()
    install_env()
