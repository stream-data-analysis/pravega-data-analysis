import yaml


class Config:
    DEFAULT_CONFIG_PATH = "config.yaml"

    def __init__(self):
        self.path_to_config = self.DEFAULT_CONFIG_PATH
        self.load_config(path_to_config=self.path_to_config)

    def load_config(self, path_to_config) -> None:
        """
        Load config with custom path
        :param path_to_config: full path to config yaml file
        :return: None
        """
        self.path_to_config = path_to_config
        stream = open(path_to_config, 'r')
        self.data = yaml.load(stream, Loader=yaml.FullLoader)

    def get_value(self, full_key, delimiter=".") -> any:
        """
        Get and return value from loaded config
        :param delimiter: delimiter on the config tree
        :param full_key: path of required option in the config tree
        :return: value of option
        """
        keys = full_key.split(delimiter)
        tmp = self.data
        for k in keys:
            tmp = tmp[k]
        return tmp
