#!/usr/bin/env python3
import argparse
import sys, os

sys.path.append(os.pardir)
sys.path.append(".")

import utils.influx_utils as influx
from utils.config_util import *
from influxdb import InfluxDBClient


class CQ():
    def __init__(self, name, body):
        self.name = name
        self.body = body


RESAMPLE = "EVERY 30s"
CQs = [

    # Segments

    CQ(
        name="segment_read_byte_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_segmentstore_segment_read_bytes \
        FROM pravega_segmentstore_segment_read_bytes \
        GROUP BY time(10s), * 
        """
    ),
    CQ(
        name="segment_write_byte_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_segmentstore_segment_write_bytes \
        FROM pravega_segmentstore_segment_write_bytes \
        GROUP BY time(10s), * 
        """
    ),
    CQ(
        name="segment_write_event_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_segmentstore_segment_write_events \
        FROM pravega_segmentstore_segment_write_events \
        GROUP BY time(10s), * 
        """
    ),
    CQ(
        name="segment_splits_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_controller_segment_splits \
        FROM pravega_controller_segment_splits \
        GROUP BY time(10s), * 
        """
    ),
    CQ(
        name="segment_merge_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_controller_segment_merges \
        FROM pravega_controller_segment_merges \
        GROUP BY time(10s), * 
        """
    ),

    # Storage

    CQ(
        name="storage_read_byte_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_segmentstore_storage_read_bytes \
        FROM pravega_segmentstore_storage_read_bytes \
        GROUP BY time(10s), * 
        """
    ),
    CQ(
        name="storage_write_byte_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_segmentstore_storage_write_bytes \
        FROM pravega_segmentstore_storage_write_bytes \
        GROUP BY time(10s), * 
        """
    ),

    # Bookkeeper

    CQ(
        name="bookkeeper_write_byte_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_segmentstore_bookkeeper_write_bytes \
        FROM pravega_segmentstore_bookkeeper_write_bytes \
        GROUP BY time(10s), * 
        """
    ),

    # Transactions

    CQ(
        name="transactions_aborted_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_controller_transactions_aborted \
        FROM pravega_controller_transactions_aborted \
        GROUP BY time(10s), * 
        """
    ),
    CQ(
        name="transactions_created_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_controller_transactions_created \
        FROM pravega_controller_transactions_created \
        GROUP BY time(10s), * 
        """
    ),
    CQ(
        name="transactions_committed_rate",
        body="""
        SELECT non_negative_derivative(last("value"), 1s) AS "Rate" \
        INTO pravega_controller_transactions_committed \
        FROM pravega_controller_transactions_committed \
        GROUP BY time(10s), * 
        """
    ),

    # Stream total rates

    CQ(
        name="stream_read_byte_rate",
        body="""
        SELECT sum("Rate") AS "Rate" \
        INTO pravega_segmentstore_stream_read_bytes \
        FROM pravega_segmentstore_segment_read_bytes \
        GROUP BY time(10s), "stream", "scope", "test_id"
        """
    ),
    CQ(
        name="stream_write_byte_rate ",
        body="""
        SELECT sum("Rate") AS "Rate" \
        INTO pravega_segmentstore_stream_write_bytes \
        FROM pravega_segmentstore_segment_write_bytes \
        GROUP BY time(10s), "stream", "scope", "test_id"
        """
    ),
    CQ(
        name="stream_write_event_rate ",
        body="""
        SELECT sum("Rate") AS "Rate" \
        INTO pravega_segmentstore_stream_write_events \
        FROM pravega_segmentstore_segment_write_events \
        GROUP BY time(10s), "stream", "scope", "test_id"
        """
    ),

]


def set_and_get_options() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Continues queries management')
    parser.add_argument('-l', '--list', action="store", metavar="DB", help='list CQs')
    parser.add_argument('-c', '--create', action="store", metavar="DB", help='create all CQs')
    parser.add_argument('-d', '--delete', action="store", metavar="DB", help='delete all CQs')
    return parser.parse_args()


def create_client(db) -> InfluxDBClient:
    config = Config()
    host = config.get_value("db.host")
    port = config.get_value("db.port")
    return influx.get_client(host=host,
                             port=port,
                             db=db)


if __name__ == '__main__':
    args = set_and_get_options()
    if args.list:
        DB = args.list
        c = create_client(DB)
        cqs = next((v[DB] for v in c.get_list_continuous_queries() if DB in v.keys()))
        for cq in cqs:
            # print('%-30s %-12s' % (cq['name'], cq['query']))
            print("pravega_segmentstore_" + cq['name'])
    elif args.create:
        DB = args.create
        c = create_client(DB)
        for cq in CQs:
            print('%-35s %-12s' % ("CQ  " + cq.name, "was created"))
            c.create_continuous_query(cq.name, cq.body, database=DB, resample_opts=RESAMPLE)
    elif args.delete:
        DB = args.delete
        c = create_client(DB)
        for cq in CQs:
            print('%-35s %-12s' % ("CQ  " + cq.name, "was deleted"))
            c.drop_continuous_query(cq.name, database=DB)
